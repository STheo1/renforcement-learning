# Dimensions de la fenêtre
WINDOW_SIZE = (800, 800)
STEP_ANGLE = 10
STEP_MOVE = 3
Q_INIT = -10000
FILE_Q = "Q_matrix"
FILE_CAR = "car.png"
FILE_ROAD = "road.png"
CAR_SIZE = (60,40)
NB_TEST_POINTS = 4

# Couleurs
WHITE = (255, 255, 255) # Super
BLACK = (0, 0, 0)
GREEN = (0, 128, 0)
RED = (255, 0, 0)
ORANGE = (255, 165, 0)
YELLOW = (255, 255, 0)
BAD_COLOR = (-1,-1,-1)

import pygame

from abc import ABC, abstractclassmethod


class Obstacle(ABC):
    
    def __init__(self, x, y, size, reward):
        self.x = x
        self.y = y
        self.raw_surface = pygame.Surface(size, pygame.SRCALPHA)
        self.surface = self.plot_surface()
        self.rect = self.surface.get_rect()
        self.rect.move_ip(self.x, self.y)
        self.mask = pygame.mask.from_surface(self.surface)

    @abstractclassmethod
    def plot_surface(self):
        return self.surface

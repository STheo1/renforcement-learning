import os
import random 
import numpy as np

from Constants import *
from Player import Player

class Qlearning :
    def __init__(self,player, nb_state, nb_action, learning_rate, discount_factor, epsilon=0.5):
        self.eps = epsilon 
        self.learning_rate = learning_rate
        self.discount_factor = discount_factor
        self.player = player #Objet de type player
        self.Q = np.matrix(np.ones((nb_state,nb_action)) * Q_INIT)
        self.convergence = False

    def Q_load(self,name):
        if os.path.isfile(name): 
          self.Q=np.load(name)
          print("Matrice Q chargée !")
        else:
          print("Je ne trouve pas la matrice",name)

    def Q_save(self,name):
        np.save(name,self.Q)
        print("Matrice Q sauvée !")

    def Q_learning(self, old_state, action, new_state, reward): 
        if old_state<0 or old_state >=len(self.Q):
            return False
        if new_state<0 or new_state >=len(self.Q):
            return False
        Q_old = np.copy(self.Q)
        self.Q[old_state, action] = (1-self.learning_rate)* self.Q[old_state, action] + self.learning_rate*(reward + self.discount_factor* np.max(self.Q[new_state]))
        if np.linalg.norm(self.Q-Q_old)<10**-3: # S'il y a convergence
            self.convergence = True
        return True

    def Q_debug(self):
        nb = 0
        init = 0
        bystates = [0]*(1<<NB_TEST_POINTS)
        for ((x,y),v) in np.ndenumerate(self.Q):
          if v==Q_INIT:
            init+=1
            bystates[x%(1<<NB_TEST_POINTS)]+=1
          nb+=1
        print("Éléments de Q non touchés par l'apprentissage : ", "%.2f" % (init*100/nb), "%")
        print("Même grandeur par état de points testés :")
        for (i,v) in enumerate(bystates):
          print("* état", "0x%01x" % i, " -> ", "%.2f" % (v*33/(360//STEP_ANGLE)), "%")
        if self.convergence:
          print("La matrice Q a convergé");
        
    def best_action(self, state):
        if state<0 or state >=len(self.Q):
            return -1
        return np.argmax(self.Q[state])

import pygame

from Obstacle import Obstacle


class RoadBot(Obstacle):

    def __init__(self, x, y, size, rayon, color):
        self.rayon = rayon
        self.color = color
        self.size = size
        super().__init__(x, y, size, -1)


    def plot_surface(self):
        pygame.draw.circle(self.raw_surface, self.color, (0, self.size[0]), int(self.rayon), 0)

        return self.raw_surface

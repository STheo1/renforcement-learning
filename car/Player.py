import math
import pygame
import random

from Obstacle import Obstacle
from Constants import *

class Player(Obstacle):

    def __init__(self, x, y, angle, size, imgfile):
        self.size = size
        self.angle = angle
        self.load(imgfile)
        super().__init__(x, y, size, 0)

    def place(self, x, y, angle):
        if x>=0:
          self.x = x
        else:
          self.x = random.randrange(0, WINDOW_SIZE[0])
        if y>=0:
          self.y = y
        else:
          self.y = random.randrange(0, WINDOW_SIZE[1])
        if angle>=0:
          self.angle = angle
        else:
          self.angle = random.randrange(0, 360, STEP_ANGLE)

    def plot_surface(self):
        pygame.draw.rect(self.raw_surface, RED, (0, 0, self.size[0], self.size[1]), 1)
        return self.raw_surface

    def plot(self,surface):
	# voiture tournée
        rotated = pygame.transform.rotate(self.car, self.angle)
        # rectangle de la voiture tournée
        box = rotated.get_rect()
        # rectangle centré sur la position
        box = box.move(self.x-box.width//2, self.y-box.height//2)
        # affiche les points de test en orange
        pixels = self.get_pixels()
        now = (self.x,self.y)
        surface.set_at(self.add_pixels(pixels['bc'],now),ORANGE);
        surface.set_at(self.add_pixels(pixels['fl'],now),ORANGE);
        surface.set_at(self.add_pixels(pixels['fr'],now),ORANGE);
        surface.set_at(self.add_pixels(pixels['fc'],now),ORANGE);
        if NB_TEST_POINTS>=5:
          surface.set_at(self.add_pixels(pixels['tl'],now),ORANGE);
        if NB_TEST_POINTS>=6:
          surface.set_at(self.add_pixels(pixels['tr'],now),ORANGE);
        if NB_TEST_POINTS>=7:
          surface.set_at(self.add_pixels(pixels['tb'],now),ORANGE);
        if NB_TEST_POINTS>=8:
          surface.set_at(self.add_pixels(pixels['ts'],now),ORANGE);
        surface.blit(rotated,box)

    def load(self, name):
        self.car = pygame.image.load(name);
        self.car = pygame.transform.scale(self.car,self.size)

    def move(self, action):
        """
        Fonction faisant bouger le joueUr en fonction de l'action
        0 -> avancer tout droit
        1 -> avancer en tournant vers la droite
        2 -> avancer en tournant vers la gauche
        """
        out=False

        if action==1:
            # La voiture tourne sur la droite
            self.angle -= STEP_ANGLE
            self.angle %= 360

        if action==2:
            # La voiture tourne sur la gauche
            self.angle += STEP_ANGLE
            self.angle %= 360

        # La voiture avance
        inc_x=STEP_MOVE*math.cos(math.radians(self.angle))
        inc_y=STEP_MOVE*math.sin(math.radians(self.angle))
        if 0<=self.x+inc_x<WINDOW_SIZE[0] and 0<=self.y-inc_y<WINDOW_SIZE[1]:
          self.x += inc_x
          self.y -= inc_y
        else:
          out=True

        return out

    def rotate_pixel(self, x, y, angle):
        r=math.radians(self.angle)
        nx=x*math.cos(r)+y*math.sin(r)
        ny=-x*math.sin(r)+y*math.cos(r)
        return (int(nx),int(ny))

    def add_pixels(self,p1,p2):
        return (int(p1[0]+p2[0]),int(p1[1]+p2[1]))

    def get_pixels(self):
        pbc=self.rotate_pixel(-1.25*self.size[0]//2,0,self.angle)
        pfl=self.rotate_pixel(1.25*self.size[0]//2,-1.25*self.size[1]//2,self.angle)
        pfr=self.rotate_pixel(1.25*self.size[0]//2,1.25*self.size[1]//2,self.angle)
        pfc=self.rotate_pixel(1.75*self.size[0]//2,0,self.angle)
        tuple = { 'bc' : pbc, 'fl' : pfl, 'fr' : pfr, 'fc' : pfc } 
        if NB_TEST_POINTS>=5: 
          pts=self.rotate_pixel(-self.size[0]//2,-1.5*self.size[1],self.angle)
          tuple['ts'] = pts
        if NB_TEST_POINTS>=6: 
          ptl=self.rotate_pixel(self.size[0]//2,-3*self.size[1],self.angle)
          tuple['tl'] = ptl
        if NB_TEST_POINTS>=7: 
          ptr=self.rotate_pixel(self.size[0]//2,3*self.size[1],self.angle)
          tuple['tr'] = ptr
        if NB_TEST_POINTS>=8: 
          ptb=self.rotate_pixel(-self.size[0]//2,3*self.size[1],self.angle)
          tuple['tb'] = ptb
        return tuple

    def get_color(self, surface, pixel):
        rect=surface.get_rect()
        p1x=rect.x
        p1y=rect.y
        p2x=rect.x+rect.width-1
        p2y=rect.y+rect.height-1
        if p1x<=pixel[0]<=p2x and p1y<=pixel[1]<=p2y:
          return surface.get_at(pixel)[:3]
        else:
          return BAD_COLOR 

    def get_testpoints_colors(self, surface):
        pixels = self.get_pixels();
        now = (self.x,self.y)
        # Il y a NB_TEST_POINTS points de test
        colors=[
          self.get_color(surface,self.add_pixels(pixels['bc'],now)),
          self.get_color(surface,self.add_pixels(pixels['fl'],now)),
          self.get_color(surface,self.add_pixels(pixels['fr'],now)),
          self.get_color(surface,self.add_pixels(pixels['fc'],now))
          ]
        if NB_TEST_POINTS>=5:
          colors.append(self.get_color(surface,self.add_pixels(pixels['ts'],now)))
        if NB_TEST_POINTS>=6:
          colors.append(self.get_color(surface,self.add_pixels(pixels['tl'],now)))
        if NB_TEST_POINTS>=7:
          colors.append(self.get_color(surface,self.add_pixels(pixels['tr'],now)))
        if NB_TEST_POINTS>=8:
          colors.append(self.get_color(surface,self.add_pixels(pixels['tb'],now)))
        return colors

    def is_offroad(self, surface):
        colors=self.get_testpoints_colors(surface)
        green = 0
        for c in colors:
          if c==GREEN or c==BAD_COLOR:
            green+=1
        if green==len(colors):
          return True
        else:
          return False

    def is_onroad(self, surface):
        colors=self.get_testpoints_colors(surface)
        green = 0
        for c in colors:
          if c==GREEN or c==BAD_COLOR:
            green+=1
        if green==0:
          return True
        else:
          return False

    def is_between(self, surface):
        colors=self.get_testpoints_colors(surface)
        green = 0
        for c in colors:
          if c==GREEN or c==BAD_COLOR:
            green+=1
        if green==0 or green==len(colors):
          return False
        else:
          return True

    def get_reward(self, surface):
        colors=self.get_testpoints_colors(surface)
        bad = 0
        better = 0
        verygood = 0
        i = 0
        for c in colors:
          if i<5:
            if c==GREEN or c==BAD_COLOR:
              bad += 1
          else:
            if c==GREEN or c==BAD_COLOR:
              better += 1
          if c==WHITE:
            verygood += 1
          i+=1
        if bad>0:
          return -1000
        if verygood>0:
          return 5000
        if better>0:
          return 2000
        return 1000
    
    def get_state(self,surface):
        colors=self.get_testpoints_colors(surface)
        state=0
        power=1
        for c in colors:
          if c==GREEN:
            state += power
          power *= 2
        state += power*(self.angle//STEP_ANGLE)
        return state


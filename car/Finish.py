import pygame

from Obstacle import Obstacle


class Finish(Obstacle):

    def __init__(self, x, y, size, color):
        self.color = color
        self.size = size
        super().__init__(x, y, size, -1)


    def plot_surface(self):
        pygame.draw.rect(self.raw_surface, self.color, pygame.Rect(5/6 * self.size[1], self.size[0]*0.96, 1/6 * self.size[1], self.size[0]*0.04))

        return self.raw_surface
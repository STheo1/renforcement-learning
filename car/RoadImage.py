import os
import pygame

from Obstacle import Obstacle

class RoadImage(Obstacle):

    def __init__(self, x, y, size, name):
        self.size = size
        if os.path.isfile(name):
          self.image = pygame.image.load(name);
          self.image = pygame.transform.scale(self.image,self.size)
        else:
          self.image = None
        super().__init__(x, y, size, -1)

    def plot_surface(self):
        if self.image != None:
          self.raw_surface.blit(self.image,self.image.get_rect())
        return self.raw_surface

import pygame
import os
import numpy as np

from RoadTop import RoadTop
from RoadBot import RoadBot
from RoadImage import RoadImage
from Finish import Finish
from Q_learning import Qlearning
from Player import Player
from Constants import *

class Game:

    def __init__(self, player, Q, road_path):
        # On crée la fenêtre sur laquelle on joue
        pygame.init()
        self.window = pygame.display.set_mode(WINDOW_SIZE)
        pygame.display.set_caption("Course")

        # On ajoute une surface verte à cette fenêtre
        self.screen = pygame.Surface(WINDOW_SIZE)
        self.screen.fill(GREEN)

        # Création du jeu et du circuit
        self.player = player
        if os.path.isfile(road_path):
          road = RoadImage(0, 0, WINDOW_SIZE, road_path)
          self.obstacles = [ road ]
        else:
          self.obstacles = [
            RoadTop(0, 0, WINDOW_SIZE, WINDOW_SIZE[0], BLACK), 
            RoadBot(0, 0, WINDOW_SIZE, WINDOW_SIZE[0] - WINDOW_SIZE[1] / 6, GREEN),
            Finish(0, 0, WINDOW_SIZE, WHITE)
          ]

        #Q
        self.Q=Q


    def add_element(self, obstacle):
        self.obstacles.append(obstacle)

    def run(self):
        exploration=False
        clock = pygame.time.Clock()

        # Lancement du jeu
        running = True
        old_state = -1
        old_action = -1
        skip = True
        first = True
        debug = False
        once = True
        learning=False
        old_x = self.player.x
        old_y = self.player.y
        old_angle = self.player.angle
        while running:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False
                if event.type == pygame.KEYDOWN:
                    skip=False
                    once=False
                if event.type == pygame.KEYUP :
                    skip=True
            if not first and not exploration and skip:
                continue    
            first = False

            # Affichage des différents éléments
            self.window.blit(self.screen, (0, 0))
            for obstacle in self.obstacles:
                self.window.blit(obstacle.surface, obstacle.rect)

            # Commandes pour l'utilisateur
            keys = pygame.key.get_pressed()
            if not once and keys[ord("x")]:
                exploration = True
            if not once and keys[ord("w")]:
                exploration = False
                print("L'utilisateur déplace la voiture par les touches ci-dessous :")
                print("* touche 'z' -> avance tout droit")
                print("* touche 'd' -> avance et tourne à droite")
                print("* touche 'q' -> avance et tourne à gauche")
                print("* touche 'n' -> utilise l'action apprise (matrice Q)")
            if not once and keys[ord("v")]:
                debug = not debug
                if debug:
                  print("Déverminage activé")
                else:
                  print("Déverminage désactivé")
            if not once and keys[ord("a")]:
                learning = not learning
                if learning:
                  print("Apprentissage activé (mise à jour de Q et exploration automatique)")
                else:
                  debug = False
                  print("Apprentissage désactivé")

            new_state = -1;
            if not exploration:
                # Diriger la voiture via des touches
                action=-1
                if keys[ord("z")]:
                    action=0
                if keys[ord("d")]:
                    action=1
                if keys[ord("q")]:
                    action=2
                if keys[ord("n")]:
                    action=self.Q.best_action(old_state)
                if action>=0:
                  self.player.move(action)
                if debug:
                  print("Action effectuée :",action)
            else :
                # Diriger la voiture avec la matrice d'apprentissage
                if learning:
                  bestchoice = np.random.choice([True,False], p=[0.1, 0.9])
                else:
                  bestchoice = True
                if bestchoice:
                    action = self.Q.best_action(old_state)
                else :
                    if old_action>=0:
                      action = np.random.choice([old_action,0,1,2], p=[0.7, 0.1, 0.1, 0.1])
                    else:
                      action = np.random.choice([0,1,2], p=[0.2, 0.4, 0.4])
                if action<0:
                    exploration = False
                else:
                    out=self.player.move(action)
                    if out or self.player.is_offroad(self.window):
                      if learning:
                        while True:
                          self.player.place(-1,-1,-1)
                          if learning and self.player.is_between(self.window):
                            break
                      else:
                        self.player.place(old_x,old_y,np.random.choice([340,350,0,10,20]))
                      old_state=-1
            new_state=self.player.get_state(self.window)

            # Apprentissage
            if learning:
              if old_state>=0 and action>=0:
                # Calcul le reward de la position du joueur
                reward=self.player.get_reward(self.window)
                # Met à jour la matrice d'apprentissage
                self.Q.Q_learning(old_state, action, new_state, reward)
                if debug:
                  print('état       -> ', "0x%04x" % new_state)
                  print('récompense -> ', reward)
              else:
                if action>=0:
                 self.Q.Q_debug()

            old_state = new_state
            old_action = action

            # Ajout du joueur sur la fenêtre
            self.player.plot(self.window);

            # Affichage
            pygame.display.flip()
            clock.tick(30)
            once = True

        pygame.quit()

def main():
    print("Voiture autonome, version avec", NB_TEST_POINTS, "points de tests")
    print("  la sélection de ce nombre se fait dans Constants.py de 4 à 8") 
    print("Commandes :")
    print(" * activer/désactiver l'apprentissage (désactivé par défaut)")
    print("   -> touche 'a'")
    print(" * mode conduite par l'utilisateur (mode par défaut)")
    print("   -> touche 'w' (tapez 'w' pour avoir la liste des touches de direction)")
    print(" * mode conduite automatique avec la matrice Q")
    print("   -> touche 'x'")
    print(" * activer/désactiver le déverminage (désactivé par défaut)")
    print("   -> touche 'v'")
    dir_path = os.path.dirname(os.path.realpath(__file__))
    player = Player(50, 90, 0, CAR_SIZE, dir_path+"/"+FILE_CAR)
    Q = Qlearning(player, int(360//STEP_ANGLE*(1<<NB_TEST_POINTS)), 3, 0.25, 0.5)
    Q.Q_load(dir_path+"/"+FILE_Q+'.npy')
    game = Game(player, Q, dir_path+"/"+FILE_ROAD)
    game.run()
    Q.Q_save(FILE_Q)

if __name__ == "__main__":
    pygame.mixer.quit() 
    main()

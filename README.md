# RL Self-Driving Car

## Description

This project implements a reinforcement learning (RL) based self-driving car simulator. Using Q-learning, an RL algorithm, the simulated car learns to navigate through various obstacles to reach a specified destination, which is located at the bottom right of the map. The simulation environment includes roads and grass to challenge and train the RL agent.

## Features

- **Simulation Environment**: Customizable road and obstacle settings.
- **Reinforcement Learning**: Utilizes Q-learning for autonomous decision-making.
- **Real-time Learning Visualization**: Observe the learning process as the car navigates through the environment.
- **Modular Design**: Separate modules for the player (car), obstacles, and the learning algorithm, facilitating easy customization.

## Getting Started

### Prerequisites

Ensure you have Python 3.6 or newer installed on your system. Dependencies include `numpy`, and you may need `pygame` for rendering the simulation environment.

### Installation

1. Clone the repository to your local machine:
   ```
   git clone https://gitlab.com/STheo1/renforcement-learning.git
   ```
2. Install required Python packages:
   ```
   pip install numpy pygame
   ```

### Usage

To start the simulation and training process, run the `main.py` script:
```
python3 car/main.py
```
The project comes with an already computed Q-matrix which is loaded at program launch. You may want to start the car in automatic mode, this is done by pressing the 'x' key.

If you want more control to test the learned behaviour, keep the car in manual mode ('w' key). You are then able to drive the car using the learned moves by pressing the
'n' key. You may also want to disturb the trajectory by taking control of the car with 'q' (forward left), 'd' (forward right) and 'z' (straight forward) keys. You are
then able to resume learned moves using the 'n' key again.

Now for testing the learning algorithm you can remove the Q_matrix.npy file to start with an empty matrix. You only have to launch the learning mode with the 'a' key. In
manual mode you will have to drive the car all over the map. This is quite fastidious so we advise to switch to automatic learning by pressing the 'x' key just after the
'a' key.  Since nothing usefull happen in the grass, each time the car is lost in the green or try to escape the map, the trajectory is ended and a summary of the Q matrix
is displayed.

During the learning process you can display some verbose information with the 'v' key. For each move the state and the reward are given. The reward is a mere integer.
The state is given in hexadecimal to make it more easily understandable. The least significant bits reflect the state of the car sensors (the right most digit if the 
program run with four sensors). The most significant bits reflect the car direction (36 different angles are used with the default angle step stored in the Constants.py file).

Note that the Q matrix is automaticaly saved if the python program is gracefully stopped (using the close icon of the graphical window). So the learning can be resumed 
by a later run of the program since the Q matrix is also automaticaly loaded if the file Q_matrix.npy is present in the project directory.

The best results were obtained using three sensors in front of the car. The position of the car sensors are displayed using orange pixels. The rear sensor is quite useless. 
A version with long range sensors (4 more test points) was tried (and is still usable by extending the number of test points to 8 in the Constants.py file) but it needed a
lot more learning time without a much better result. The idea was that those long range sensors should be able to keep the car near the grass to make the car moving in the
left lane.

The road we drawed for our first tests is quite primitive. We were eager to test the algorithm on a more complex one. So we made a quick hack to load a road from a picture
instead of drawing it with python graphic primitives. The more complex road is in the sub-directory "harder". You can move the picture from carv/harder/road.png to
carv/road. The new road will then be loaded by the program. You may test the actual learning on this road. And you will see the car crashing off-road. We were a bit
disapointed but after making the car learning again on this map the result was far better. You can test with the new Q matrix just by moving car/harder/Q_matrix.npy to 
car/Q_matrix.npy.

## Visuals



![Screen capture simple road](Route-simple-demonstration.mp4)

This screen capture shows the self-driving car handling the curved road of our first tests.

![Screen capture complex road more learning](qualifying_lap.mp4)

This time the behaviour of the car is far better on the complex road after some more learning on this new map.
